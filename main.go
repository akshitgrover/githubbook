package main

import (
	"githubbook/routes"
	"net/http"
)

func main() {
	go http.HandleFunc("/details", routes.Repos)
	go http.HandleFunc("/", routes.GetName)
	go http.Handle("/file/", http.StripPrefix("/file", http.FileServer(http.Dir("./assets"))))
	go http.HandleFunc("/favicon.ico", routes.Favicon)
	http.ListenAndServe(":8080", nil)
}
