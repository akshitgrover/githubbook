package routes

import (
	"encoding/json"
	//"fmt"
	"log"
	"net/http"
	"os"
	"text/template"
)

type Repo struct {
	Name     string `json:"full_name`
	Url      string `json:"html_url"`
	Language string `json:"language"`
	Stars    int    `json:"stargazers_count"`
}

type Text struct {
	Login      string `json:"login"`
	Name       string `json:"name"`
	Image      string `json:"avatar_url"`
	Url        string `json:"html_url"`
	Followers  int    `json:"followers"`
	Followring int    `json:"following"`
	Company    string `json:"company"`
	Bio        string `json:"bio"`
	ReposCount int    `json:"public_repos"`
	Repos      []Repo
	Language   map[string]int
	Message    string `json:"Message"`
}

type Token struct {
	Access_token string
}

var tplErr *template.Template
var tplDet *template.Template
var tplHome *template.Template

func init() {
	tplErr = template.Must(template.ParseFiles("./views/error.gohtml", "./views/header.gohtml"))
	tplDet = template.Must(template.ParseFiles("./views/details.gohtml", "./views/header.gohtml"))
	tplHome = template.Must(template.ParseFiles("./views/home.gohtml", "./views/header.gohtml"))
}

func Repos(w http.ResponseWriter, req *http.Request) {
	println(req.Method + " " + req.URL.Path)
	if req.Method != "GET" {
		w.WriteHeader(405)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Invalid Request Method")
		return
	}
	var flag Text
	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(400)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Error Processing Request")
		return
	}

	f, err := os.Open("./routes/config.json")
	if err != nil {
		log.Panic(err)
	}
	var fi os.FileInfo
	fi, err = f.Stat()
	if err != nil {
		log.Panic(err)
	}
	bs := make([]byte, fi.Size())
	f.Read(bs)
	var token Token
	json.Unmarshal(bs, token)

	res, err := http.Get("https://api.github.com/users/" + req.Form["username"][0] + "?access_token=" + token.Access_token)
	if err != nil {
		w.WriteHeader(500)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Error Occured While Making API Calls")
		return
	}
	dec := json.NewDecoder(res.Body)
	defer res.Body.Close()
	err = dec.Decode(&flag)
	if err != nil {
		w.WriteHeader(400)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Error Occured While Decoding JSON")
		log.Panic(err)
		return
	} else if flag.Name == "" {
		if flag.Message == "Not Found" {
			w.WriteHeader(404)
			tplErr.ExecuteTemplate(w, "error.gohtml", "No User Found")
			return
		}
		w.WriteHeader(400)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Api Requests Excedded")
		return
	}
	res, err = http.Get("https://api.github.com/users/" + req.Form["username"][0] + "/repos?access_token=" + token.Access_token)
	if err != nil {
		w.WriteHeader(500)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Something Went Wrong")
		log.Panic(err)
		return
	}
	dec = json.NewDecoder(res.Body)
	dec.Token()
	if err != nil {
		w.WriteHeader(404)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Something Went Wrong")
		log.Panic(err)
		return
	}
	var rf Repo
	var rff []Repo
	lang := make(map[string]int)
	var count int
	for dec.More() {
		err = dec.Decode(&rf)
		if err != nil {
			w.WriteHeader(404)
			tplErr.ExecuteTemplate(w, "error.gohtml", "Something Went Wrong")
			log.Panic(err)
			return
		}
		rff = append(rff, rf)
		lang[rf.Language] += 1
		count += 1
	}
	flag.Repos = rff
	for k, v := range lang {
		lang[k] = int((float32(v) / float32(count)) * 100)
	}
	flag.Language = lang
	dec.Token()
	w.WriteHeader(200)
	tplDet.ExecuteTemplate(w, "details.gohtml", flag)
}

func Favicon(w http.ResponseWriter, req *http.Request) {
	println(req.Method + " " + req.URL.Path)
	if req.Method != "GET" {
		w.WriteHeader(405)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Invalid Request Method")
		return
	}
	http.ServeFile(w, req, "./assets/images/favicon.png")
}

func GetName(w http.ResponseWriter, req *http.Request) {
	println(req.Method + " " + req.URL.Path)
	if req.Method != "GET" {
		w.WriteHeader(405)
		tplErr.ExecuteTemplate(w, "error.gohtml", "Invalid Request Method")
		return
	}
	tplHome.ExecuteTemplate(w, "home.gohtml", nil)
}
